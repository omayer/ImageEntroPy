# ImageEntroPy

ImageEntroPy is a python tool for calculating the information-theoretic entropy in 
digital images. 

Entropy is a measure of average information content. In image processing, 
entropy is used as a texture descriptor. Low entropy images have a "flat" appearance and 
high entropy images have a "textured" appearance. 

While designed with images in mind, this tool can also be used on any symbolized data.
Symbols can be integers, bits or characters, but not floating point data.


## Requirements
python 3,
numpy
## Usage
Here's a basic working example of calculating entropy in an image

```python
import entropy
import matplotlib.pyplot as plt

I = plt.imread('./examples/test2.jpg') #load image
h = entropy.iment(I) #calculate entropy of image

```


Please checkout the following examples for usage demonstrations and output:

- [Entropy calculations on natural image tiles](examples/example_natural_image_tiles.ipynb)
- [Entropy calculations on synthetic images](examples/example_synthetic_images.ipynb)
- [Entropy calculations on time-series data](examples/example_timeseries.ipynb)



## Theory

[Wikipedia page on information-theoretic entropy](https://en.wikipedia.org/wiki/Entropy_(information_theory))

Entropy is often thought of as the average information conveyed by a single unit of data. For example, take two files: a 100MB file, and a 1MB compressed version of that same file. The data contained in the 1MB file has greater entropy since one byte of that file conveys 100 times more information, on average, than one byte in the uncompressed version. Entropy can also be thought of as a measure of disorder. In this example, the compression algorithm removes predictable (i.e. ordered) parts of the file, resulting in a highly disordered, high entropy compressed file.

In images, entropy is often used as a texture descriptor. Highly disordered (e.g. textured) regions have high entropy. Flat regions have low entropy.

Here are some examples image tiles.

![Example image tiles](doc/imagetiles.png "Example image tiles")


Entropy, H(X), is calculated by the equation:

![Entropy Equation](doc/entropyeq.png "Entropy equation")

where P(x\_i) is the proability that a data unit x takes the ith value of n possible values. b is the base that is used, we often use the natural log resulting in entropy units called "nats." In images, P(x\_i) is the proability that a pixel takes the value i, with i an integer from 0 to 255 (e.g. typically an unsigned 8 bit integer). Entropy is maximized for uniform distibutions of x e.g. P(x\_i)=1/n for all i, and minimized when P(x_i)=1 for any i.


## Contributing
Owen Mayer


# -*- coding: utf-8 -*-
"""
author: Owen Mayer
email: om82@drexel.edu
created: 2018 July 24

This module contains functions for calculating the information-theoretic entropy
of various data formats. Intended for use on uint8 images, but can be used on 
any symbolized (i.e. not floating point data).
"""

import numpy as np


def entropy(V,units='nats'):
    #Calculate the information theoretic entropy of vector V
    #Vector V has dimension N x C, where:
    #   N = number of observations of symbols
    #   C = length of symbol
    #For images, N is the number of pixels in an image and C=1 for gray-scale
    #images, or C=3 for color (RGB) images.

    #So far only tested on integer symbols.
    #This operation doesn't make sense for floating point numbers.

    #Return counts of unique symbols
    #Note: according to this result: https://stackoverflow.com/a/43096495,
    #np.unique is the most efficient method to provide unique counts
    #outside of np.bincount. However, np.bincount requires integer input and
    #would break for generic symbolized data. For len(V) > 10^4, np.bincount
    #is 1-2 orders of magnitude faster. #Might be worth putting a switch in.    
    symbs,counts = np.unique(V,axis=0,return_counts=True)

    #total number of unique symbols
    total = counts.sum()

    #"probability" of each symbol
    p = counts/float(total)

    #Calculate entropy (in nats)
    H = -1*((p*np.log(p)).sum())

    #Convert units
    if (units.lower() == 'nats'):
        return H

    elif (units.lower() == 'bits') | (units.lower() == 'shannons'):
        return H/np.log(2)

    elif (units.lower() == 'dits') | (units.lower() == 'bans') | (units.lower() == 'hartleys'):
        return H/np.log(10)

    elif (units.lower() == 'bims'): #Black and white 8 bit IMage
        return H/np.log(256)

    elif (units.lower() == 'cims'): #Color (3-channel) 8 bit IMage
        return H/np.log(256**3)

    else:
        raise ValueError('Unknown entropy unit type {}'.format(units))


def iment(I,units='nats',conversion=None):
    #Entropy for an image X with 2 spatial dimensions + 1 optional channel dimension
    #Channel dimension needs to be last
    #For an input image X with height M, width N, and C color channels

    if conversion == 'rgb2gray':
        #luminance conversion (same standard used as matlab)
        I = np.dot(I[...,:3], [0.299, 0.587, 0.114]).astype(np.uint8)

    elif conversion is not None:
        raise ValueError('Unknown image conversion type {}'.format(conversion))

    #Convert array of dimension (M,N,C) to (M*N,1) vector of symbols with dimension (1xC)
    V = I.reshape(I.shape[0]*I.shape[1],-1) #collapse 1st and 2nd dimensions.

    #calculate entropy
    H = entropy(V,units)
    return H


def iment_batch(listI,units='nats',conversion=None):

    #calculate entropy for each tile
    listH = []
    for I in listI:
        H = iment(I,units=units,conversion=conversion)
        listH.append(H)

    arrayH = np.array(listH)
    return arrayH

